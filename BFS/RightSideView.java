

//https://leetcode.com/problems/binary-tree-right-side-view/submissions/

import java.util.*;

public class RightSideView {
	
   public static List<Integer> rightSideView(TreeNode root) {
	   
	   List<Integer> rigthNodes = new ArrayList<>();
	   if (root == null)
		   return rigthNodes;
	   
	   ArrayDeque<TreeNode> queue = new ArrayDeque<>();
	   queue.add(root);
	   
	   while (!queue.isEmpty()) {
		   int qSize = queue.size();
		   for (int i=0; i<qSize; i++) {
			   TreeNode currNode = queue.poll();
			   if (i==0)
				   rigthNodes.add(currNode.val);
			   
			   if (currNode.right != null)
				   queue.offer(currNode.right);
			   
			   if (currNode.left != null)
				   queue.offer(currNode.left);
		   }
	   }
	   
	   return rigthNodes;
        
    }

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(2);
		root.left.right = new TreeNode(5);
		root.right = new TreeNode(3);
		root.right.right = new TreeNode(4);

		List<Integer> list = rightSideView(root);
		for (Integer no: list)
			System.out.print(no + " ");
	}

}
