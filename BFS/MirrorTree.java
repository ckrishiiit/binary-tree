
//https://leetcode.com/problems/symmetric-tree/

public class MirrorTree {
	
    public static boolean isSymmetric(TreeNode root) {
        
    	return isSymmetric(root, root);
    }
    
    private static boolean isSymmetric(TreeNode t1, TreeNode t2) {
    	
    	if (t1 == null && t2 == null)
    		return true;
    	
    	if (t1 == null || t2 == null)
    		return false;
    	
    	return t1.val == t2.val && isSymmetric(t1.left, t2.right)
    			                && isSymmetric(t2.right, t1.left);
    	
    }
    

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.left.left = new TreeNode(3);
        root.left.right = new TreeNode(4);
        
        root.right = new TreeNode(2);
        root.right.left = new TreeNode(4);
        root.right.right = new TreeNode(3);
        
        System.out.println(isSymmetric(root));
	}

}
