
import java.util.List;
import java.util.ArrayList;
import java.util.ArrayDeque;

//https://leetcode.com/problems/binary-tree-level-order-traversal/

class LevelOrderTraversal {
	
		public List<List<Integer>> levelOrder(TreeNode root) {

			List<List<Integer>> result = new ArrayList<List<Integer>>();

			if (root == null)
				return result;

			ArrayDeque<TreeNode> queue = new ArrayDeque<TreeNode>();
			queue.offer(root);

			while (!queue.isEmpty()) {

				int queueSize = queue.size();
				List<Integer> level = new ArrayList<>(queueSize);
				for (int i = 0; i < queueSize; i++) {
					TreeNode currentNode = queue.poll();
					int currentVal = currentNode.val;

					level.add(currentVal);

					if (currentNode.left != null)
						queue.offer(currentNode.left);

					if (currentNode.right != null)
						queue.offer(currentNode.right);

				}
				result.add(level);
			}

			return result;
		}
	}

