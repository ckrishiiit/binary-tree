import java.util.LinkedList;
import java.util.*;

//https://leetcode.com/problems/binary-tree-level-order-traversal-ii/

public class ReverseLevelOrder {
	
    public static List<List<Integer>> levelOrderBottom(TreeNode root) {
        
        List<List<Integer>> result = new ArrayList<List<Integer>>();

        if (root == null)
          return result;

          ArrayDeque<TreeNode> queue = new ArrayDeque<>();
          queue.offer(root);

          while (!queue.isEmpty()) {

             int qSize = queue.size();
             List<Integer> level = new LinkedList<>();

             for (int i=0; i < qSize; i++) {

                    TreeNode currentNode = queue.poll();
                    int currentVal = currentNode.val;
                    level.add(currentVal);

                    if (currentNode.left != null)
                      queue.offer(currentNode.left);

                    if (currentNode.right != null)
                      queue.offer(currentNode.right);

             }
//Adding at index 0. so that last level added at 0. Adding at the begin in list is
// Less expensives
             result.add(0,level);
          }
          
        return result;
    }
	
	public static void main(String a[]) {

		TreeNode root = new TreeNode(3);
		root.left = new TreeNode(9);
		root.right = new TreeNode(20);
		root.right.left = new TreeNode(15);
		root.right.right = new TreeNode(7);
		
		List<List<Integer>> list = levelOrderBottom(root);
		
		for (List<Integer> level : list) {
			for (Integer no :level) {
				System.out.print(no +" ");
			}
			System.out.println();
		}
	}
}
