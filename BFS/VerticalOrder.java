
import java.util.*;
 

public class VerticalOrder {
	
	static class VerticalNode {
		TreeNode node;
		int row;
		int col;
		
		public VerticalNode(TreeNode node, int row, int col) {
			this.node = node;
			this.row = row;
			this.col = col;
		}
	}
	
	 public static List<List<Integer>> verticalTraversal(TreeNode root) {
		 
		 List<List<Integer>> result = new ArrayList<List<Integer>>();
		 
		 if (root == null)
			 return result;
		 
		 ArrayDeque<VerticalNode> queue = new ArrayDeque<>();
		 queue.offer(new VerticalNode(root, 0,0));
		 
		 TreeMap<Integer, TreeMap<Integer, PriorityQueue<Integer>>> map = new TreeMap<>();
		 
		 while (!queue.isEmpty()) {
			 
			 int qSize = queue.size();
			 
			 for (int i=0; i<qSize; i++) {
				 VerticalNode curr = queue.poll();
				 int row = curr.row;
				 int col = curr.col;
				 
				 map.putIfAbsent(row, new TreeMap<>());
				 map.get(row).putIfAbsent(col, new PriorityQueue<>());
				 
				 map.get(row).get(col).offer(curr.node.val);
				 
				 if (curr.node.left != null)
					 queue.offer(new VerticalNode(curr.node.left, row-1, col+1));
				 
				 if (curr.node.right != null)
					 queue.offer(new VerticalNode(curr.node.right, row+1, col+1));
			 }
			 
		 }
		 
		 //iterating over map
		 
		for  (TreeMap<Integer, PriorityQueue<Integer>> pq : map.values()) {
			List<Integer> list = new ArrayList<>();
			
			for (PriorityQueue<Integer> nodes: pq.values()) {
				while (!nodes.isEmpty()) {
					list.add(nodes.poll());
				}
			}
			result.add(list);	
		}
		 return result;
	        
	  }

	public static void main(String[] args) {
	
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(2);
		root.right = new TreeNode(3);
		root.left.left = new TreeNode(4);
		root.left.right = new TreeNode(6);
		root.right.left = new TreeNode(5);
		root.right.right = new TreeNode(7);
		
		List<List<Integer>> res = verticalTraversal(root);
		for (List<Integer> list: res) {
			System.out.println(list);
		}

	}

}
